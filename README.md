# Setting up Development env
## Installation of Vagrant, Virtual box and Ruby

## Using Vagrant
### vagrant commands
- `vagrant up` - to launch the vm
- `vagrant destroy` - deletes everything
- `vagrant reload` - to reload any new instructions to the `vagrantfile`
- `vagrant halt` - to stop the VM
- `vagrant status` - checks the status of the VM

More commands can be found by typing `vagrant` on the GitBash terminal
```
vagrant
Common commands:
     box             manages boxes: installation, removal, etc.
     cloud           manages everything related to Vagrant Cloud
     destroy         stops and deletes all traces of the vagrant machine
     global-status   outputs status Vagrant environments for this user
     halt            stops the vagrant machine
     help            shows the help for a subcommand
     hostsupdater
     init            initializes a new Vagrant environment by creating a Vagrantfile
     login
     package         packages a running vagrant environment into a box
     plugin          manages plugins: install, uninstall, update, etc.
     port            displays information about guest port mappings
     powershell      connects to machine via powershell remoting
     provision       provisions the vagrant machine
     push            deploys code in this environment to a configured destination
     rdp             connects to machine via RDP
     reload          restarts vagrant machine, loads new Vagrantfile configuration
     resume          resume a suspended vagrant machine
     snapshot        manages snapshots: saving, restoring, etc.
     ssh             connects to machine via SSH
     ssh-config      outputs OpenSSH valid configuration to connect to the machine
     status          outputs status of the vagrant machine
     suspend         suspends the machine
     up              starts and provisions the vagrant environment
     upload          upload to machine via communicator
     validate        validates the Vagrantfile
     version         prints current and latest Vagrant version
     winrm           executes commands on a machine via WinRM
     winrm-config    outputs WinRM configuration to connect to the machine
```
For help on any individual command run `vagrant COMMAND -h`

### Running the virtual machine (VM) and testing the nginx web-server
Using `vagrant ssh`, the VM can be accessed
- The `apt-get` Linux package manager is used to install and uninstall any packages needed
- To use the command in `admin` mode we can use `sudo` before the command
- `sudo apt-get update -y` - Updates the package listings
- `sudo apt-get upgrade -y` - Upgrades any available packages installed
- `ping www.bbc.co.uk` - Pings a url e.g, `bbc.co.uk`
- `sudo apt-get install name_of_package` - Installs a package
- To work in an `admin mode` at all times (not recommended) use `sudo -su`

Installing nginx in the guest machine/VM/ubuntu 16.04
- To launch the default nginx page in host machine's browser
- Install nginx using `sudo apt-get install nginx -y`
- `systemctl status nginx` - used to check  status and clarify installations
- `systemctl restart nginx` - restarts nginx package
- `systemctl start nginx` - starts the nginx package if its stopped or not running

- To run provision.sh, file permissions need to be given to Linux and make this file Executable
- To change permission we use `chmod` with required permission the file name `chmod +x provision.sh`

To create a VM the dependencies we need are vagrant and virtual box and gitbash

### Coding a provision file so that dependencies are installed automatically without intervention

- In the `provision.sh` file
```shell
!#bin/bash
# Updates the package list that may need upgrading
sudo apt-get update -y
# Fetch new versions of packages existing on the machine
sudo apt-get upgrade -y
# Installs nginx package
sudo apt-get install nginx -y
# Checks to see if nginx is running properly
systemctl status nginx
# updates nodejs package for upgrade
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
# Updates the nodejs into higher version
sudo apt-get install nodejs -y
# Using npm to install pm2 package
sudo npm install -g pm2
# Navigates the program to the app folder
cd app
# Runs the app.js node so the web server is running
node app.js
```

If everything runs fine the web server should be running and the console with output a message:
`default: Your app is ready and listening on port 3000`

To close the web server or exit the program `Ctrl` + `c` can be used.

Vagrant has a method of syncing folders from the host OS to the guest virtual machine.
- The `env` command shows all the variables in use
- A variable can be defined by typing `export variable_name='variable'`
-The variable can be checked by using the `print env` command followed by the variable name
   - `printenv variable_name`

When the VM is exited or shut down, the variable isn't saved.
To permanently store a variable, it needs to be implemented in the `.bashrc` file

We can use the echo command to put it into the .bashrc file
- `sudo echo 'export DB_HOST=mongodb' >> .bashrc`

To rerun the .bashrc file, we can use:
- `source ~/.bashrc`


### Reverse Proxy

#### Finding the default file for nginx server
- Navigate to `/etc`
- Then `nginx` folder
- Finally `sites-available` folder
- using `ls`, the default file should be showing
- using `sudo nano default` the file can be edited
- alternatively `sudo nano /etc/nginx/sites-available/default` can be used

In the default file, this code is used to navigate the port to the home page at port 80
```
server {
      listen 80;
      server_name _;
      location / {
              proxy_pass http://192.168.10.100:3000;
              proxy_http_version 1.1;
              proxy_set_header Upgrade $http_upgrade;
              proxy_set_header Connection 'upgrade';
              proxy_set_header Host $host;
              proxy_cache_bypass $http_upgrade;
      }
}
```
- Using `sudo nginx -t` the configuration of the file can be checked
- To restart nginx, `sudo systemctl restart nginx` is used
- To re-run the reverse proxy, first navigate to where the app.js file is stored e.g., in the `/app/` folder and run `node app.js`

### Multi-Machine / Multi-Server Environment

#### Setting up multiple machines
- Vagrantfile
```
Vagrant.configure("2") do |config|

  config.vm.define "db" do |db|
    db.vm.box = "ubuntu/xenial64"
    db.vm.network "private_network", ip: "192.168.10.150"
    db.vm.provision "shell", path: "./environment/db-provision.sh"
  end

  config.vm.define "app" do |app|
    app.vm.box = "ubuntu/xenial64"
    app.vm.synced_folder "environment", "/home/vagrant/env"
    app.vm.synced_folder "app", "/home/vagrant/app"
    app.vm.network "private_network", ip: "192.168.10.100"
    app.hostsupdater.aliases = ["development.local"]
    app.vm.provision "shell", path: "./environment/provision.sh"
  end
end
```
- Using the separated config code blocks, two VMs can be created with one vagrant file

#### Setting up the app
- `vagrant ssh db` to access `db` VM
- Open the `mongod.conf` file for editing by using `sudo nano /etc/mongod.conf`
- find the `bindIp` and change it to `0.0.0.0`
- save by pressing `Ctrl` + `X`
- exit the `db` VM
- enter the `app` VM using `vagrant ssh app`
- if it's not changed in the `provision.sh` file, use `sudo echo 'export DB_HOST=mongodb://192.168.10.150:27017/posts' >> .bashrc`
- Run the source file by using `source ~/.bashrc`
- Check if the variable is correct using `printenv DB_HOST`
- navigate to `app` folder and run `npm start`
- If it runs but /posts doesn't work, run `node seeds/seed.js` in the app folder
- If it doesn't run at all, use `ps aux` and check for any `npm` or `node app.js` processes running and kill them by using `sudo kill` followed by the process number

For easier automation, we can incorporated these commands into the provision file
- Updated provision.sh file
```shell
!#bin/bash
# Updates the package list that may need upgrading
sudo apt-get update -y
# Fetch new versions of packages existing on the machine
sudo apt-get upgrade -y
# Installs nginx package
sudo apt-get install nginx -y
# Checks to see if nginx is running properly
systemctl status nginx
# updates nodejs package for upgrade
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
# Installs the nodejs and npm packages
sudo apt-get install nodejs -y
# Installs python dependencies
sudo apt-get install python-software-properties -y
# Using npm to install pm2 package
sudo npm install -g pm2
# Edits the default file for the correct proxy_pass
cd /etc/nginx/sites-available/
sudo rm -rf /default
sudo echo "server{
        listen 80;
        server_name _;
        location / {
        proxy_pass http://192.168.10.100:3000;
        proxy_http_version 1.1;
        proxy_set_header Upgrade \$http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host \$host;
        proxy_cache_bypass \$http_upgrade;
        }
}" >> default
# Restarts nginx
sudo systemctl restart nginx
sudo systemctl enable nginx
# Reseeds the database and runs the web-server
node /home/vagrant/app/seeds/seed.js
node /home/vagrant/app/app.js
```

We can also automatically install mongodb on the `db` VM
- The `db` provision file - `db-provision`
```shell
!#bin/bash
# Updates the mongodb package so it can be upgraded
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 –recv D68FA50FEA312927
echo "deb https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
# Updates the package list that may need upgrading
sudo apt-get update -y
# Fetch new versions of packages existing on the machine
sudo apt-get upgrade -y
# Installs mongodb package
sudo apt-get install -y mongodb-org=3.2.20 mongodb-org-server=3.2.20 mongodb-org-shell=3.2.20 mongodb-org-mongos=3.2.20 mongodb-org-tools=3.2.20 --allow-unauthenticated
# Changing the mongod.conf file with right bindIp
cd /etc
sudo rm -rf mongod.conf
sudo echo "
storage:
  dbPath: /var/lib/mongodb
  journal:
    enabled: true
systemLog:
  destination: file
  logAppend: true
  path: /var/log/mongodb/mongod.log
net:
  port: 27017
  bindIp: 192.168.10.150
" >> mongod.conf
# Restarting mongodb
sudo systemctl restart mongod
sudo systemctl enable mongod
```

The env variable `DB_HOST` can also be automatically added by using this command on the provision line in the `Vagrantfile`
```shell
, env: {'DB_HOST' => 'mongodb://192.168.10.150:27017/posts'}
```

So it should look like this:
```shell
app.vm.provision "shell", path: "provision.sh", env: {'DB_HOST' => 'mongodb://192.168.10.150:27017/posts'}
```
