!#bin/bash
# Updates the package list that may need upgrading
sudo apt-get update -y
# Fetch new versions of packages existing on the machine
sudo apt-get upgrade -y
# Installs nginx package
sudo apt-get install nginx -y
# Checks to see if nginx is running properly
systemctl status nginx
# updates nodejs package for upgrade
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
# Installs the nodejs and npm packages
sudo apt-get install nodejs -y
# Installs python dependencies
sudo apt-get install python-software-properties -y
# Using npm to install pm2 package
sudo npm install -g pm2
# Edits the default file for the correct proxy_pass
cd /etc/nginx/sites-available/
sudo rm -rf /default
sudo echo "server{
        listen 80;
        server_name _;
        location / {
        proxy_pass http://192.168.10.100:3000;
        proxy_http_version 1.1;
        proxy_set_header Upgrade \$http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host \$host;
        proxy_cache_bypass \$http_upgrade;
        }
}" >> default
# Restarts nginx
sudo systemctl restart nginx
sudo systemctl enable nginx
# Reseeds the database and runs the web-server
node /home/vagrant/app/seeds/seed.js
node /home/vagrant/app/app.js
